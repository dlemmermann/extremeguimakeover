package com.extreme;

public enum Genre {
    ACTION,
    DRAME,
    SCIENCE_FICTION,
    COMEDY;
}
