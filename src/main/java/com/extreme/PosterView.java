package com.extreme;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

public class PosterView extends StackPane{

    private final ImageView imageView;

    public PosterView() {
        getStyleClass().add("poster-view");

        imageView = new ImageView();
        imageView.fitHeightProperty().bind(prefHeightProperty());
        imageView.setPreserveRatio(true);
        StackPane.setAlignment(imageView, Pos.TOP_LEFT);

        getChildren().add(imageView);

        movie.addListener(it -> updatePoster());

        updatePoster();
    }

    private void updatePoster() {
        Movie movie = getMovie();
        if (movie == null) {
            imageView.setImage(null);
        } else {
            imageView.setImage(new Image(ExtremeGUI.class.getResource("/" + movie.getPosterFileName()).toExternalForm()));
        }
    }

    private final ObjectProperty<Movie> movie = new SimpleObjectProperty<>(this, "movie");

    public final ObjectProperty<Movie> movieProperty() {
        return movie;
    }

    public final Movie getMovie() {
        return movie.get();
    }

    public final void setMovie(Movie movie) {
        this.movie.set(movie);
    }
}
